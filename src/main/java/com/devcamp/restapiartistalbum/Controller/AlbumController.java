package com.devcamp.restapiartistalbum.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapiartistalbum.models.Album;
import com.devcamp.restapiartistalbum.service.AlbumService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AlbumController {
  @Autowired
  private AlbumService albumService;

  @GetMapping("/album-info")
  public Album getAlbumInfor(@RequestParam(name = "albumId") int albumId) {
    return albumService.getAlbumId(albumId);
  }

}
